# AboutABook

## Created by

* Nicholas Trevino
* Terence Wong

---

## Overview

AboutaBook is a Application designed to allow the user to manage books and journals of the currently signed in user. It allows the user to create, view, Update and delete journals and books.

## Features

- **Book Mangement**: Allows the user to create, view, update and delete books. Fearture is hidden behind authentication and authorization to allow for privacy of data created by user.

- **Journal Mangement**: Allows the user to create, view, update and delete journal as long the user has already made a book to store journal underneath.Fearture is hidden behind authentication and authorization to allow for privacy of data created by user.

## Technologies

- React.js for frontend development
- FastAPI for backend development
- PostgreSQL for database management
- JWT for user authentication
- Tailwind CSS for styling and responsive design
- Material Tailwind for styling
- Boostrap Icons for styling

---

## How to Install and Run the Project

### Requirements

- Docker
- Docker Compose
- Git

### Downloading and Installing the Project

Follow the steps below to download the project from Git and install it using Docker:

**Step 1: Clone the Repository**

To clone the repository, open a terminal, and run the following command:

```
git clone https://gitlab.com/Ntrevino12/aboutabook.git
```

**Step 2: Change to the Project Directory**

Change your working directory to the root of the cloned project:

```
cd aboutabook
```

**Step 3: Build and Run the Docker Container**

To build and run the Docker container, use the following commands:
These commands will create the volume, build the Docker image and run the containers in the background.

```
docker volume create bookdata
docker-compose up
```

**Step 4: Access the API**

The API should now be accessible at http://localhost:8000. You can interact with the API using the provided FastAPI interactive documentation at http://localhost:8000/docs.

**Stopping and Removing the Docker Container**

To stop the running container, run the following command:

```
docker-compose down
```

This command will stop and remove the container. If you want to rebuild and start the container again, you can use the command from Step 3.

---

## Database Schema


---

## Api Endpoints

**Accounts**

**Books**

**Journals**


---

## Models
