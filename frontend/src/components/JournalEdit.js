import { useState } from "react";

function JournalEdit({ token, journal, closeEditPopUp }) {
  const [id] = useState(journal.id);
  const [user_id] = useState(journal.user_id);
  const [book_id] = useState(journal.book_id);
  const [summary, setSummary] = useState(journal.summary);

  //handles submission of all edit forms for journals
  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {};
    data.id = id;
    data.user_id = user_id;
    data.book_id = book_id;
    data.summary = summary;

    const journalUrl = `${process.env.REACT_APP_API_HOST}/api/journals/${id}`;
    const fetchConfig = {
      method: "PUT",
      body: JSON.stringify(data),
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(journalUrl, fetchConfig);

    if (response.ok) {
      await response.json();
      setSummary("");
      //   closeEditPopUp();
    }
  };

  //closes the edit form
  const handleClose = () => {
    // pass as a prop
    closeEditPopUp();
  };

  return (
    <>
      <div className="text-center">
        <h1 className="pt-[120px]">Edit your Journal</h1>
        <button
          className="close-btn p-3 rounded-lg text-white bg-[#402A1E] absolute top-3 right-3"
          onClick={handleClose}
        >
          X
        </button>
        <div className="max-w-[500px] pt-[90px] m-auto px-2 py-16 w-full">
          <div className="col-span-3 h-auto shadow-xl shadow-gray-400 rounded-xl lg:p-4">
            <div className="py-4">
              <form onSubmit={handleSubmit}>
                <div className="grid lg:grid-cols-1 gap-4 py-2">
                  <div className="form-floating">
                    <p>
                      <label htmlFor="summary">Summary</label>
                    </p>
                    <input
                      type="text"
                      className="form-control border-2 rounded-lg p-2 w-[250px] border-gray-300"
                      id="summary"
                      value={summary}
                      onChange={(e) => setSummary(e.target.value)}
                      required
                      placeholder=""
                    />
                  </div>
                </div>
                <button className=" py-3 px-4 rounded-lg shadow-md text-white bg-[#402A1E]">
                  Submit
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default JournalEdit;
