import React, { useState, useEffect, useContext } from "react";
import { NavLink, useNavigate, Link } from "react-router-dom";
import useToken, { AuthContext } from "@galvanize-inc/jwtdown-for-react";
import useUser from "../UseUser";

// nav not working hidding at bigger sizes and unhidding at smaller
const Nav = () => {
  const [shadow, setShadow] = useState(false);
  // eslint-disable-next-line
  const [navBg, setNavBg] = useState("#402A1E");
  // eslint-disable-next-line
  const [linkColor, setLinkColor] = useState("#F2AA52");
  const { token } = useContext(AuthContext);
  const { logout } = useToken();
  const navigate = useNavigate();
  const user = useUser(token);

  // logout properly and navigating back to home unless page is reloaded
  const handleLogout = () => {
    navigate("/");
    logout();
    window.location.reload(false);
  };
  useEffect(() => {
    const handleShadow = () => {
      if (window.scrollY >= 90) {
        setShadow(true);
      } else {
        setShadow(false);
      }
    };
    window.addEventListener("scroll", handleShadow);
  }, []);

  return (
    <div
      style={{ backgroundColor: `${navBg}` }}
      className={
        shadow
          ? "fixed w-full h-20 shadow-xl z-[100] ease-in-out duration-300"
          : "fixed w-full h-20 z-[100]"
      }
    >
      <div className="flex justify-between items-center w-full h-full px-2 2xl:px-16">
        <div>
          {token && user ? (
            <ul style={{ color: `${linkColor}` }} className="hidden 2xl:flex">
              <li className="ml-10 text-md uppercase hover:border-b">
                <NavLink to="/">Home</NavLink>
              </li>
              <li className="ml-10 text-md uppercase hover:border-b">
                <NavLink to="/books">Books</NavLink>
              </li>
              {/* <li className="ml-10 text-md uppercase hover:border-b">
              <Link href="/#skills">Journals</Link>
            </li> */}
              <li className="ml-10 text-md uppercase hover:border-b">
                <NavLink>
                  <button className="text-[#F2AA52]" onClick={handleLogout}>
                    Logout
                  </button>
                </NavLink>
              </li>
            </ul>
          ) : null}
          {token === null || user === null ? (
            <ul style={{ color: `${linkColor}` }} className="hidden 2xl:flex">
              <li className="ml-10 text-md uppercase hover:border-b">
                <NavLink to="/Login">Login</NavLink>
              </li>
              <li className="ml-10 text-md uppercase hover:border-b">
                <NavLink to="/SignUp">SignUp</NavLink>
              </li>
            </ul>
          ) : null}
        </div>
        <div>
          <Link to="/">
            <h2 className="text-[#F2AA52]">AboutABook</h2>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Nav;
