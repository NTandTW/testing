import JournalEdit from "./JournalEdit";
import Popup from "./Popup";
import { useState } from "react";
import JournalForm from "./journalform";
import JournalMenu from "./JournalMenu";

function JournalList(props) {
  const [selectedJournal, setSelectedJournal] = useState(null);
  // Pop up for Journal edit form
  const [isEditOpen, setIsEditOpen] = useState(false);

  // Delete journals by journal id
  const handleDelete = async (journal_id) => {
    const deleteUrl = `${process.env.REACT_APP_API_HOST}/api/journals/${journal_id}`;
    const config = {
      method: "delete",
      headers: { Authorization: `Bearer ${props.token}` },
    };
    const response = await fetch(deleteUrl, config);
    if (response.ok) {
      await props.getJournals(props.book_id);
    }
  };

  const openEditPopUp = (journal) => {
    setSelectedJournal(journal);
    setIsEditOpen(true);
  };

  const closeEditPopUp = () => {
    setIsEditOpen(false);
  };

  // Pop up for Journal Form
  const [isFormOpen, setIsFormOpen] = useState(false);

  const openJournalForm = () => {
    setIsFormOpen(true);
  };
  const closeJournalForm = () => {
    setIsFormOpen(false);
  };

  return (
    <div>
      {props.journals ? (
        <div>
          <div className="pt-2">
            <ul className="grid gap-[1%] md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3 2xl:grid-cols-6">
              {props.journals.map((item) => {
                return (
                  <li
                    key={item.id}
                    className="max-w-lg rounded overflow-hidden shadow-lg border-4 border-[#402A1E]"
                  >
                    <div className="flex justify-end">
                      <JournalMenu
                        handleDelete={handleDelete}
                        openEditPopUp={openEditPopUp}
                        item={item}
                      />
                    </div>
                    <div className="px-4 py-3">
                      <div className="font-bold text-xl mb-2">
                        {item.summary}
                      </div>
                      <p className="text-gray-700 text-base">{item.book_id}</p>
                    </div>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      ) : (
        <div>
          <h1> You Need To create a Journal</h1>
        </div>
      )}
      {isEditOpen && (
        <Popup trigger={true}>
          <JournalEdit
            token={props.token}
            journal={selectedJournal}
            closeEditPopUp={closeEditPopUp}
          />
        </Popup>
      )}
      <div className="fixed bottom-0 left-0 right-0 p-4 bg-[#402A1E]">
        <div style={{ display: "flex", justifyContent: "flex-end" }}>
          <button
            className="bg-gray-300 hover:bg-gray-400 text-black font-bold py-2 px-4 rounded"
            onClick={openJournalForm}
          >
            Add a Journal
          </button>
        </div>
      </div>
      {isFormOpen && (
        <Popup trigger={true}>
          <JournalForm
            token={props.token}
            book_id={props.jBook_id}
            closeJournalForm={closeJournalForm}
            user={props.user}
          />
        </Popup>
      )}
    </div>
  );
}
export default JournalList;
