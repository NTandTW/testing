import { useState } from "react";

function JournalForm({ token, book_id, closeJournalForm, user }) {
  // const [bookId, setBookId] = useState("");
  const [summary, setSummary] = useState("");

  // NEED TO WORK ON HANDLE CHANGE, SUBMIT NOT WORKING
  const handleSummaryChange = (e) => {
    const value = e.target.value;
    setSummary(value);
  };

  // Create journal for specific book
  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {};
    data.user_id = user.id;
    data.book_id = book_id;
    data.summary = summary;
    console.log(data);

    const journalUrl = `${process.env.REACT_APP_API_HOST}/api/books/${book_id}/journals`;
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(journalUrl, fetchConfig);

    if (response.ok) {
      // await response.json();
      setSummary("");
    } else {
      console.error("Could not add journal, please try again");
    }
  };

  // Pass as a prop from JournalList to close form
  const handleClose = () => {
    closeJournalForm();
  };

  return (
    <>
      <div className="text-center">
        <h1 className="pt-[120px]">Create a new Journal</h1>
        <button
          className="close-btn p-3 rounded-lg text-white bg-[#402A1E] absolute top-3 right-3"
          onClick={handleClose}
        >
          X
        </button>
        <div className="max-w-[500px] pt-[90px] m-auto px-2 py-16 w-full">
          <div className="col-span-3 h-auto shadow-xl shadow-gray-400 rounded-xl lg:p-4">
            <div className="py-4">
              <form onSubmit={handleSubmit} id="create-journal-form">
                <div className="grid lg:grid-cols-1 gap-4 py-2">
                  <div className="form-floating">
                    <p>
                      <label htmlFor="summary">Summary</label>
                    </p>
                    <input
                      type="text"
                      className="form-control border-2 rounded-lg p-2 w-[250px] border-gray-300"
                      id="summary"
                      value={summary}
                      onChange={handleSummaryChange}
                      required
                      placeholder=""
                    />
                  </div>
                </div>
                <button className=" py-3 px-4 rounded-lg shadow-md text-white bg-[#402A1E]">
                  Submit
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default JournalForm;
