import { useState, useEffect, useContext } from "react";
import { AuthContext } from "@galvanize-inc/jwtdown-for-react";
import { useParams } from "react-router-dom";
import JournalList from "./components/JournalList";
import useUser from "./UseUser";

function Journal() {
  const book = useParams();
  const [journals, setJournals] = useState([]);
  const { token } = useContext(AuthContext);
  const user = useUser(token);

  const getJournals = async () => {
    const journalListUrl = `${process.env.REACT_APP_API_HOST}/api/books/${book.id}/journals`;
    const config = {
      method: "get",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };
    const response = await fetch(journalListUrl, config);

    if (response.ok) {
      const data = await response.json();
      setJournals(data);
    }
  };

  useEffect(() => {
    getJournals();
    // eslint-disable-next-line
  }, []);

  return (
    <div className="pt-[95px]">
      <div>
        <JournalList
          book_id={book.id}
          journals={journals}
          token={token}
          user={user}
          getJournals={getJournals}
        />
      </div>
    </div>
  );
}
export default Journal;
