import { memo } from "react";
import HomePageCarousel from "./components/HomePageCarousel";

// Need to add a feature for the front page so its not so empty

export default function HomePage() {
  return (
    <div className="pt-[75px] relative">
      <HomePageCarousel />
      <div className="mx-40 flex justify-center">
        <div className="bg-[#402A1E] h-screen border-rose border-2 2xl:w-[1300px] xl:w-[1000px] lg:w-[800px] md:w-[600px]">
          <div className="flex justify-center mx-20 ">
            <h1 className="text-[#F2AA52] border-[#F2AA52] border-b-2 pt-10">
              Welcome to AboutABook
            </h1>
          </div>
          <p className="text-align-center text-lg pt-8 font-semibold mx-5">
            "Welcome to AboutABook, your digital haven for crafting and
            organizing your literary adventures. Whether you're an avid reader,
            a dedicated reviewer, or a passionate writer, our application is
            designed to be your personalized space for curating and sharing your
            book journals. With seamless features tailored to your reading
            preferences, AboutABook is here to transform the way you engage with
            literature, one page at a time."
          </p>
        </div>
      </div>
    </div>
  );
}

export const MHomePage = memo(HomePage);
