import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import MHomePage from "./HomePage";
import Nav from "./components/Nav";
import Journal from "./Journal";
import Footer from "./components/Footer";

const Book = React.lazy(() => import("./Book"));
const Login = React.lazy(() => import("./Login"));
const SignUp = React.lazy(() => import("./SignUp"));

function App() {
  const appStyle = {
    display: "flex",
    flexDirection: "column",
    minHeight: "100vh",
  };
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");
  const baseUrl = process.env.REACT_APP_API_HOST;
  return (
    <BrowserRouter basename={basename}>
      <React.Suspense fallback={<p>loading page ...</p>}>
        <AuthProvider baseUrl={baseUrl}>
          <Nav />
          <div style={appStyle}>
            <div>
              <Routes>
                <Route path="/" element={<MHomePage />} />
                <Route path="books" element={<Book />} />
                <Route path="Login" element={<Login />} />
                <Route path="SignUp" element={<SignUp />} />
                <Route path="journals/:id" element={<Journal />} />
              </Routes>
            </div>
          </div>
          <Footer />
        </AuthProvider>
      </React.Suspense>
    </BrowserRouter>
  );
}

export default App;
