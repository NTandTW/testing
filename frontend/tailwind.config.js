/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
    "./**/@material-tailwind/**/*.{html,js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    screens: {
      '2xl': {'max': '3840px'},
      // => @media (max-width: 1535px) { ... }

      'xl': {'max': '1920px'},
      // => @media (max-width: 1279px) { ... }

      'lg': {'max': '1279px'},
      // => @media (max-width: 1023px) { ... }

      'md': {'max': '1023px'},
      // => @media (max-width: 767px) { ... }

      'sm': {'max': '767px'},
      // => @media (max-width: 639px) { ... }
    },
    extend: {},
  },
  plugins: [],
}
