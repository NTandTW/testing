from pydantic import BaseModel
from queries.pool import pool
from typing import List, Optional


class JournalIn(BaseModel):
    user_id: int
    book_id: int
    summary: str


class JournalOut(BaseModel):
    id: int
    user_id: int
    book_id: int
    summary: str


class JournalAll(BaseModel):
    user_id: int
    book_id: int


class JournalQueries:
    def create(self, user_id: int, book_id: int, journal: JournalIn) -> JournalOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO journals
                            (
                               user_id
                               , book_id
                               , summary
                            )
                        VALUES ( %s, %s, %s)
                        RETURNING id ;
                        """,
                        [
                            user_id,
                            book_id,
                            journal.summary,
                        ],
                    )
                    id = result.fetchone()[0]
                    old_data = journal.dict()
                    return JournalOut(id=id, **old_data)
        # except Exception:
        #     return{"message": "couldnt Create Journal for book"}

        except Exception as e:
            print(f"Error: {e}")
            return {"message": "Could not create Journal for the book"}

    def get_one(self, user_id: int, journal_id: int) -> JournalOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id,
                        user_id,
                        book_id,
                        summary
                        FROM journals
                        WHERE id = %s
                        AND user_id = %s
                        """,
                        [
                            journal_id,
                            user_id,
                        ],
                    )
                    record = db.fetchone()
                    if record is None:
                        return None
                    return JournalOut(
                        id=record[0],
                        user_id=record[1],
                        book_id=record[2],
                        summary=record[3],
                    )
        except Exception:
            return {"message": "Couldnt get journal for book"}

    def delete(self, id: int, user_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE from journals
                        WHERE id = %s AND user_id = %s
                        """,
                        [id, user_id],
                    )
                    return True
        except Exception:
            return False

    def get_book_journals(
        self, journal: JournalAll, user_id: int, book_id: int
    ) -> List[JournalOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT j.id, j.user_id, j.book_id, j.summary
                        FROM journals j
                        JOIN books b ON j.book_id = b.id
                        WHERE j.book_id = %s
                        AND j.user_id = %s
                        """,
                        [book_id, user_id],
                    )
                    journals = []
                    for record in db:
                        journal = JournalOut(
                            id=record[0],
                            user_id=record[1],
                            book_id=record[2],
                            summary=record[3],
                        )
                        journals.append(journal)
                    return journals
        except Exception as e:
            return [{"message": "Couldn't get journals for book"}]

    def get_all_journals(
        self, journal: JournalAll, user_id: int, book_id: int
    ) -> List[JournalOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT j.id, j.user_id, j.book_id, j.summary
                        FROM journals j
                        WHERE j.user_id = %s
                        """,
                        [book_id, user_id],
                    )
                    journals = []
                    for record in db:
                        journal = JournalOut(
                            id=record[0],
                            user_id=record[1],
                            book_id=record[2],
                            summary=record[3],
                        )
                        journals.append(journal)
                    return journals
        except Exception as e:
            return [{"message": "Couldn't get journals for book"}]

    def update_journal_entry(
        self, user_id: int, jouranl_id: int, journal: JournalOut
    ) -> JournalOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE journals
                            set summary = %s
                        WHERE id = %s AND user_id = %s
                        """,
                        [journal.summary, jouranl_id, user_id],
                    )
                    old_data = journal.dict()
                    old_data["id"] = jouranl_id
                    old_data["user_id"] = user_id
                    return JournalOut(**old_data)
        except Exception:
            return {"message": "Couldnt delete Journal"}
