
fastapi[all]==0.92.0
pydantic==1.10.9
uvicorn==0.22.0
psycopg[binary,pool]==3.1.4
jwtdown-fastapi==0.5.0
