steps = [
    [
        """
        CREATE TABLE account(
            id SERIAL PRIMARY KEY NOT NULL,
            username VARCHAR(30) NOT NULL UNIQUE,
            hashed_password TEXT NOT NULL,
            email TEXT NULL
        );
        """,
        """
        DROP TABLE account;
        """,
    ]
]
