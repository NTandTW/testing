steps = [
    [
        """
        CREATE TABLE books(
            id SERIAL PRIMARY KEY NOT NULL,
            user_id INT NOT NULL,
            title VARCHAR(100) NOT NULL,
            author VARCHAR(30)
        );
        """,
        """
        DROP TABLE book;
        """,
    ]
    ]
