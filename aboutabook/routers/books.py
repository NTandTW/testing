from fastapi import APIRouter, Depends, Response
from queries.books import (
    BookIn,
    BookOut,
    BookQueries,
)
from typing import List, Optional
from authenticator import authenticator

router = APIRouter()

@router.post("/api/books", response_model=BookOut)
def create_book(
    info: BookIn,
    repo: BookQueries = Depends(),
    account_data=Depends(authenticator.get_current_account_data),
):
    user_id = account_data["id"]
    return repo.create(info, user_id)

@router.get("/api/books", response_model=List[BookOut])
def get_books(
    repo: BookQueries = Depends(),
    account_data=Depends(authenticator.get_current_account_data),
):
    user_id = account_data["id"]
    return repo.get_all(repo, user_id)

@router.get("/api/books/{book_id}", response_model=BookOut)
def get_one_book(
    book_id: int,
    repo: BookQueries = Depends(),
    account_data=Depends(authenticator.get_current_account_data),
):
    user_id =account_data["id"]
    return repo.get_one(user_id, book_id)

@router.delete("/api/books/{id}", response_model=bool)
def delete(
    id: int,
    repo: BookQueries = Depends(),
    account_data=Depends(authenticator.get_current_account_data),
):
    user_id=account_data["id"]
    return repo.delete(id, user_id)

@router.put("/api/books/{book_id}", response_model=BookOut)
def update_book(
    book_id: int,
    book: BookIn,
    repo: BookQueries = Depends(),
    account_data=Depends(authenticator.get_current_account_data),
):
    user_id=account_data["id"]
    return repo.update(user_id, book_id, book)
